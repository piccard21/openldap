### General

Docker containers for setting up OpenLDAP & a webinterface. The LDAP server is populated by some examples.


### Installation

* create & boot docker containers
```
docker-compose up -d
```

* check if its running
```
docker ps
```
* there should be 2 containers
  * the ldap server on port 389
  * the webinterface on port 8090
 
```
 CONTAINER ID        IMAGE                       COMMAND                 CREATED             STATUS              PORTS                           NAMES
7715eb90f274        osixia/phpldapadmin:0.7.2   "/container/tool/run"   3 seconds ago       Up 3 seconds        443/tcp, 0.0.0.0:8090->80/tcp   ldap_ldap_server_admin_1
c3e411d6f861        ldap_ldap_server            "/container/tool/run"   3 seconds ago       Up 2 seconds        0.0.0.0:389->389/tcp, 636/tcp   ldap_ldap_server_1
```

* for changing the ldap structure or data edit **lda/bootstrap.ldif**
  * afterwards you have to rebuild the containers
```
docker-compose down -v
docker-compose up -d --build
```
### TEST
* goto your webinterface [http://127.0.0.1:8090/](http://127.0.0.1:8090)
* Login DN: **cn=admin,dc=piccard,dc=intern**
* PW: **Test1234**
 

